#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <signal.h>
#include <termios.h>
#include "linkedlist.h"
#include "tokenizer.h"
#include "map.h"
#include "string_functions.h"

int execute_args(struct linked_list *args1,struct linked_list *args2,char* in, char* out,int is_background)
{
  if((args1 == NULL) || (args1->size == 0)) return 0;
      char *argv1[args1->size+1];
      char *argv2[(args2==NULL)?0:args2->size+1];
      argv1[args1->size] = '\0';
      if(args2!=NULL) argv2[args2->size] = '\0';
      int status1;
      int status2;
      int fd_in;
      int fd_out;
      int pipe_in = -1;
      int pipe_out = -1;
      int pipe_des[2];
      int controlling_terminal = open("/dev/tty",O_RDWR,0);
      if(controlling_terminal == -1){
        perror("tried to open controlling terminal");
        return -1;
      }
      pid_t pid1,wpid1,pid2,wpid2;
      pid1 = -2;
      pid2 = -2;
      if(args2 != NULL){
	if(pipe(pipe_des)){//pipe failed
	  perror("pipe");
	  if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	  return -1;
	} else {
	  pipe_out = pipe_des[0];
	  pipe_in = pipe_des[1];
	}
      }
      int count = 0;
      struct node *head = args1->head;
      while(head != NULL){
	argv1[count] = head -> string;
	count++;
	head = head->next;
      }
      if(args2!=NULL){
	count = 0;
	head = args2 -> head;
	while(head != NULL){
	  argv2[count] = head -> string;
	  count++;
	  head = head->next;
	}
      }
      pid1 = fork();
      if(pid1 == -1){ //fork() 1 failed
	perror("fork 1");
	if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	return -1; //did not execute, prompt for next input
      }
      if(pid1 != 0) setpgid(pid1,pid1);
      if(pid1 != 0){//parent
	if(!is_background){//wait for child 1 to finish
	  if(tcsetpgrp(controlling_terminal,pid1)==-1){
	    perror("tcsetpgrp to pid1");
	    if(close(controlling_terminal)!= 0)perror("closing the terminal file");
	    return -1;
	  }
	  do{ //wait for child 1
	    wpid1 = waitpid(pid1, &status1, WUNTRACED | WCONTINUED);
	    if(wpid1 == -1){//wait failed
	      perror("waitpid");
	      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	      return -1; //failure
	    }
	    if(WIFEXITED(status1)){ //child exited normally
	    } else if (WIFSIGNALED(status1)){
	      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	      return -1;
	    } else if (WIFSTOPPED(status1)){
	      if(close(controlling_terminal)!=0) perror("closing the terminal file");
	      return pid1;
	    } else if (WIFCONTINUED(status1)){
	      printf("%s continued\n",argv1[0]);
	    }
	  } while(!WIFEXITED(status1) && !WIFSIGNALED(status1));
	}
	if(args2 != NULL){
	  pid2=fork();
	  if(pid2 == -1){ //fork() 2 failed
	    perror("fork 2");
	    if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	    return -1; //did not execute, prompt for next input
	  }
	  if(pid2 != 0) setpgid(pid2,pid1);
	}
      }
      
      if(pid1 == 0){ //first child
	sigset_t mask;
	sigemptyset(&mask);
	if(sigprocmask(SIG_SETMASK,&mask,NULL) != 0){
	  perror("clearing first child's process mask");
	  if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	  return -1;
	}
	setpgid(pid1, pid1); //set child PGID twice to avoid race condition
	if(in != NULL){ //input redirection
	  fd_in = (open(in,O_RDONLY,0));
	  if((fd_in==-1)||(dup2(fd_in,STDIN_FILENO)==-1)||(close(fd_in)==-1)){
	    perror("input redirection");
	    if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	    return -1;
          }
	}
	if((out != NULL)||(args2 != NULL)){
	  fd_out = (args2 == NULL) ? (open(out,O_WRONLY|O_TRUNC|O_CREAT,0644)) : pipe_in;
	  if(fd_out==pipe_in) close(pipe_out);
	  if((fd_out == -1)||(dup2(fd_out,STDOUT_FILENO)==-1)||(close(fd_out)==-1)){//file open failed
	    if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	    perror("output redirection");
	    return -1;
	  }
	}
	execvp(argv1[0],argv1);
	perror("execvp"); //if we got here, execvp failed
	if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	exit(EXIT_FAILURE);

      } else if(pid2 == 0){ //second child
	sigset_t mask;
	sigemptyset(&mask);
	if(sigprocmask(SIG_SETMASK,&mask,NULL) != 0){
	  perror("clearing second child's process mask");
	  if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	  return -1;
	}
	setpgid(pid2, pid1); //set child PGID twice to avoid race condition per APUE pg 294
	fd_in = pipe_out;
	close(pipe_in);
	if((fd_in == -1)||(dup2(fd_in,STDIN_FILENO)==-1)||(close(fd_in)==-1)){
	  perror("input redirection from pipe");
	  if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	  exit(EXIT_FAILURE);
	} else { //input redirection succeeded
	  if(out != NULL){
	    fd_out = (open(out,O_WRONLY|O_TRUNC|O_CREAT,0644));
	    if((fd_out == -1)||(dup2(fd_out,STDOUT_FILENO)==-1)||(close(fd_out)==-1)){//file open failed
	      perror("output redirection:");
	      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	      return -1;
	    }
	  }
	}
	execvp(argv2[0],argv2);
	perror("execvp"); //if we got here, execvp failed
	if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	exit(EXIT_FAILURE);
      } else if (pid1 != 0 && pid2 != 0) { //parent
	if(pipe_out > 0){
	  if(close(pipe_out) == -1){
	    perror("close pipe");
	    return -1;
	  }
	  if(close(pipe_in) == -1){
	    perror("open pipe");
	    return -1;
	  }
	}
	if(!is_background && (args2 != NULL)){
	  do{
	    wpid2 = waitpid(pid2, &status2, WUNTRACED | WCONTINUED);
	    if(wpid2 == -1){//wait failed
	      perror("waitpid");
	      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	      return -1; //failure
	    }
	    if(WIFEXITED(status2)){ //child exited normally
	    } else if (WIFSIGNALED(status2)){
      	      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	      return -1;
	    } else if (WIFSTOPPED(status2)){
	      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	      return pid1;
	    } else if (WIFCONTINUED(status2)){
	      printf("%s continued\n",argv2[0]);
	    }
	  } while(!WIFEXITED(status2) && !WIFSIGNALED(status2));
	} else { //return the PID of the backgrounded process
	  if(is_background){
	    if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	    return pid1;
	  }
	}
	if(tcsetpgrp(controlling_terminal,getpgid(0))==-1){
	  perror("tcsetpgrp to shell");
	  exit(EXIT_FAILURE);
	}
	if(close(controlling_terminal)!= 0) perror("closing the terminal file");
	return 0; //success
      }
      if(close(controlling_terminal)!= 0) perror("closing the terminal file");
      return -1; //should not get here
}

int main(){
  int controlling_terminal = open("/dev/tty",O_RDWR,0);
  if(controlling_terminal == -1){
    perror("tried to open controlling terminal");
    return -1;
  }
  sigset_t *blocked_sigs = malloc(sizeof(sigset_t)); //the shell itself should ignore SIGSTOP and SIGTERM
  if(blocked_sigs == NULL){
    perror("allocating signal set for signal mask");
    exit(EXIT_FAILURE);
  }
  sigemptyset(blocked_sigs);
  sigaddset(blocked_sigs, SIGTERM);
  sigaddset(blocked_sigs, SIGTSTP);
  sigaddset(blocked_sigs, SIGTTIN);
  sigaddset(blocked_sigs, SIGINT);
  sigaddset(blocked_sigs, SIGTTOU);
  if(sigprocmask(SIG_SETMASK,blocked_sigs,NULL) != 0){
    perror("setting shell's signal mask");
    free(blocked_sigs);
    exit(EXIT_FAILURE);
  }

  struct linked_list* argument_list;
  struct linked_list* pipe_args;
  pid_t last_backgrounded = 0;
  struct map* background = NULL;
  int bg_is_running = 0;
  while(1){
    argument_list = NULL;
    pipe_args = NULL;
    if(controlling_terminal == -1){//couldn't get controlling terminal
      perror("initial controlling terminal fetch");
      free(blocked_sigs);
      exit(EXIT_FAILURE);
    }
    if(tcsetpgrp(controlling_terminal,getpgid(0))==-1){
      perror("tcsetpgrp to shell in loop");
      free(blocked_sigs);
      close(controlling_terminal);
      exit(EXIT_FAILURE);
    }
    TOKENIZER *tokenizer = NULL;
    char buf[1025];
    write(STDOUT_FILENO,"$ ",2);
    int is_pipe = 0;
    int was_background = 0;
    char* in_filename = NULL;
    char* out_filename = NULL;
    if(tcsetpgrp(controlling_terminal,getpgid(0))==-1){
      perror("tcsetpgrp to shell");
      exit(EXIT_FAILURE);
    }
    int bytes_read = read(STDIN_FILENO,buf,1024);
    if(bytes_read<0){//read failure
      perror("read");
      exit(EXIT_FAILURE);
    }
    buf[bytes_read] = '\0';
    if((tokenizer = init_tokenizer(buf))==NULL){
      fprintf(stderr,"Out of memory.\n");
      free_tokenizer(tokenizer);
      continue;
    }
    
    char *token = get_next_token(tokenizer);
 
    if((token != NULL) && (is_valid_string(token))){
      if(string_compare(token, "bg")){
	free(token);
	token = get_next_token(tokenizer);
	free_tokenizer(tokenizer);
	if(is_valid_string(token)){
	  fprintf(stderr,"Invalid use of bg.\n");
	  free(token);
	  continue;
	}
	free(token);
	if(last_backgrounded == 0){
	  fprintf(stderr,"The most recent background job has finished.\n");
	  continue;
	} else if(!bg_is_running){
	  if(killpg(last_backgrounded, SIGCONT) == -1){
	    perror("bg");
	    continue;
	  } else { //successfully delivered SIGCONT
	    printf("Running: %s\n",get_name(last_backgrounded,background));
	    bg_is_running = 1;
	    continue;
	  }
	} else {
	  printf("The most recent background job is already running.\n");
	  continue;
	}
      }
      if(string_compare(token, "fg")){
	free(token);
	token = get_next_token(tokenizer);
	free_tokenizer(tokenizer);
	if(is_valid_string(token)){
	  fprintf(stderr,"Invalid use of fg.\n");
	  free(token);
	  continue;
	}
	free(token);
	if(last_backgrounded == 0){
	  fprintf(stderr,"The most recent background job has finished.\n");
	  continue;
	} else {
	  if(tcsetpgrp(controlling_terminal, last_backgrounded) == -1){
	    perror("fg setting foreground process group");
	    continue;
	  }
	  if(!bg_is_running){
	    if(killpg(last_backgrounded, SIGCONT) == -1){
	      perror("fg");
	      continue;
	    }
	  }
	  printf("%s\n",get_name(last_backgrounded,background));
	  bg_is_running = 1;
	  int status=0;
	  if(tcsetpgrp(controlling_terminal,last_backgrounded)==-1){
	    perror("tcsetpgrp to last backgrounded");
	    exit(EXIT_FAILURE);
	  }
	  if(waitpid(last_backgrounded,&status,WUNTRACED) == -1){
	    perror("waitpid in fg");
	    continue;
	  }
	  if(tcsetpgrp(controlling_terminal,getpgid(0))==-1){
	    perror("tcsetpgrp to shell");
	    exit(EXIT_FAILURE);
	  }
	  if(WIFEXITED(status)){
	    fprintf(stdout,"\nFinished: ");
	  } else if(WIFSTOPPED(status)){
	    fprintf(stdout,"\nStopped: ");
	  } else if(WIFCONTINUED(status)){
	    fprintf(stdout,"Continued: ");
	  } else {
	    continue;
	  }
	  printf("%s\n",get_name(last_backgrounded,background));
	  if(WIFEXITED(status)){
	    last_backgrounded = 0;
	    bg_is_running = 0;
	    map_remove(last_backgrounded,background);
	  }
	  if(WIFSTOPPED(status)) bg_is_running = 0;
	  if(WIFCONTINUED(status)) bg_is_running = 1;
	  continue;
	}
      }
      if(string_compare(token,"exit")){
	fprintf(stdout,"Exiting shell.\n");
	free(token);
	if(argument_list != NULL) erase_list(argument_list);
	if(pipe_args != NULL) erase_list(pipe_args);
	if(blocked_sigs != NULL) free(blocked_sigs);
	if(background != NULL) erase_map(background);
	free_tokenizer(tokenizer);
        break;
      }
      if((token[0] == '>')||(token[0] == '<') || (token[0] == '|') || (token[0] == '&')){
	fprintf(stderr,"Invalid input.\n");
	free_tokenizer(tokenizer);
	free(token);
	continue;
      }
      argument_list = list_init(token);
      token = get_next_token(tokenizer);
      while(is_valid_string(token)){
	  if((token[0] == '&') || was_background){ //run in background
	  free(token);
	  was_background = 1;
	  int i;
	  for(i = 0; i < bytes_read; i++){
	    if(buf[i] == '&'){
	      buf[i] = '\0';
	      break;
	    }
	  }
	  int status = execute_args(argument_list,pipe_args,in_filename,out_filename,1);
	  if(status == -1){//execution failed
	    erase_list(argument_list);
	    erase_list(pipe_args);
	    in_filename = NULL;
	    out_filename = NULL;
	  } else { //status indicates the PGID of the backgrounded process just executed
	    was_background = 1;
	    last_backgrounded = status;
	    if(background == NULL){
	      background = map_init(last_backgrounded, buf);
	    } else {
	      push_map(last_backgrounded, buf, background);
	    }
	    erase_list(argument_list);
	    erase_list(pipe_args);
	    argument_list = NULL;
	    pipe_args = NULL;
	    printf("Running: %s\n",buf);
	    bg_is_running = 1;
	    in_filename = NULL;
	    out_filename = NULL;
	  }
	  free_tokenizer(tokenizer);
	  tokenizer = NULL;
	  argument_list = NULL;
	  pipe_args = NULL;
	  break;
	}
	if(token[0] == '|'){
	  is_pipe = 1;
	  free(token);
	  token = get_next_token(tokenizer);
	  if(!(is_valid_string(token))||(token[0] == '>')||(token[0] == '<')||(token[0] == '|')){
	    fprintf(stderr,"Invalid pipe input.\n");
	    free_tokenizer(tokenizer);
	    tokenizer = NULL;
	    if(token != NULL) free(token);
	    token = NULL;
	    erase_list(argument_list);
	    argument_list = NULL;
	    break;
	  } else {//token after pipe is valid command
	    pipe_args = list_init(token);
	    token = get_next_token(tokenizer);
	    continue;
	  }
	}
	if(token[0] == '>'){ //output redirection
	  free(token);
	  token = get_next_token(tokenizer); 
	  char * token2 = get_next_token(tokenizer);
          if(!(is_valid_string(token))||(token[0] == '|')||(token[0] == '>')||(token[0] == '<')||(token[0] == '&')){ //invalid
	    fprintf(stderr,"Invalid output redirection.\n");
            free_tokenizer(tokenizer);
	    tokenizer = NULL;
            if(token != NULL) free(token);
	    token = NULL;
            if(token2 != NULL) free(token2);
            erase_list(argument_list);
	    erase_list(pipe_args);
	    argument_list = NULL;
	    pipe_args = NULL;
	    if(in_filename != NULL) free(in_filename);
	    in_filename = NULL;
	    if(out_filename != NULL) free(out_filename);
	    out_filename = NULL;
	    is_pipe = 0;
            break;
          } else {//token is the specified output file
	      if(is_valid_string(token2)){
		if(token2[0] == '&'){
		  int status = execute_args(argument_list,pipe_args,in_filename,token,1);
		  if(status > 0){
		    was_background = 1;
		    last_backgrounded = status;
		    erase_list(argument_list);
		    erase_list(pipe_args);
		    argument_list = NULL;
		    pipe_args = NULL;
		    bg_is_running = 1;
		    if(background == NULL){
		      background = map_init(last_backgrounded, buf);
		    } else {
		      push_map(last_backgrounded, buf, background);
		    }
		    printf("Running: %s\n",get_name(last_backgrounded,background)); 
		  }
		  if(in_filename != NULL) free(in_filename);
		  in_filename = NULL;
		  free_tokenizer(tokenizer);
		  tokenizer = NULL;
		  if(token != NULL) free(token);
		  token = NULL;
		  if(token2 != NULL) free(token2);
		  break;
		}
	        if((token2[0] == '>')||(token2[0]=='|')){ //invalid redirection
	          fprintf(stderr,"Invalid redirection.\n");
	          free_tokenizer(tokenizer);
		  tokenizer = NULL;
	          free(token);
		  token = NULL;
	          free(token2);
		  erase_list(argument_list);
		  erase_list(pipe_args);
		  argument_list = NULL;
		  pipe_args = NULL;
		  break;
	        } else { //token 2 after a > is something other than > or |
		  if(token2[0] == '<'){ //valid combined redirection
		    char* token3 = get_next_token(tokenizer); //hopefully this is a valid input file
	            if(!is_valid_string(token3)||(token3[0]=='|')){
		      fprintf(stderr,"Invalid input redirection.\n");
		      free_tokenizer(tokenizer);
		      tokenizer = NULL;
		      free(token);
		      token = NULL;
		      free(token2);
		      if(token3 != NULL) free(token3);
		      erase_list(argument_list);
		      erase_list(pipe_args);
		      argument_list = NULL;
		      pipe_args = NULL;
		      break;
		    } else { //token3 is the specified input file, we are redirecting both input and output
		      char* token4 = get_next_token(tokenizer);
		      if((is_valid_string(token4))&&(!string_compare(token4,"&"))){
			fprintf(stderr,"Invalid input redirection.\n");
			free_tokenizer(tokenizer);
			tokenizer = NULL;
			free(token);
			token = NULL;
			free(token2);
			free(token3);
			free(token4);
			erase_list(argument_list);
			erase_list(pipe_args);
			argument_list = NULL;
			pipe_args = NULL;
			break;
		      }
		      int status;
		      if((is_valid_string(token4))&&(string_compare(token4,"&"))){
			status = execute_args(argument_list,pipe_args,token3,token,1);
		      } else {
			status = execute_args(argument_list,pipe_args,token3,token,0);
		      }
		      if(status > 0){//sent SIGTSTP or started in background
			was_background = 1;
			last_backgrounded = status;
			erase_list(argument_list);
			erase_list(pipe_args);
			argument_list = NULL;
			pipe_args = NULL;
			if(background == NULL){
			  background = map_init(last_backgrounded, buf);
			} else {
			  push_map(last_backgrounded, buf, background);
			}
			printf("\nStopped: %s\n",buf);
		      }
		      free_tokenizer(tokenizer);
		      tokenizer = NULL;
		      free(token);
		      token = NULL;
		      free(token2);
		      free(token3);
		      if(token4 != NULL) free(token4);
		      erase_list(argument_list);
		      erase_list(pipe_args);
		      argument_list = NULL;
		      pipe_args = NULL;
		      break;
		    }
		  } else { //input is something invalid
		    fprintf(stderr,"> then something that's not < is invalid input.\n");
		    free_tokenizer(tokenizer);
		    tokenizer = NULL;
		    free(token);
		    token = NULL;
	            free(token2);
		    erase_list(argument_list);
		    erase_list(pipe_args);
		    argument_list = NULL;
		    pipe_args = NULL;
		    break;
		  }
		}
	      } else { //token2 is invalid or null so we are just redirecting output to token, not input
	        int status = execute_args(argument_list,pipe_args,in_filename,token,0);
		if(status > 0){//sent SIGTSTP
		  was_background = 1;
		  last_backgrounded = status;
		  erase_list(argument_list);
		  erase_list(pipe_args);
		  if(background == NULL){
		    background = map_init(last_backgrounded, buf);
		  } else {
		    push_map(last_backgrounded, buf, background);
		  }
		  printf("\nStopped: %s\n",buf);
		  argument_list = NULL;
		  pipe_args = NULL;
		  bg_is_running = 0;
		} else {
		  erase_list(argument_list);
		  erase_list(pipe_args);
		}
		if(in_filename != NULL) free(in_filename);
		in_filename = NULL;
		free_tokenizer(tokenizer);
		tokenizer = NULL;
	        if(token != NULL) free(token);
		token = NULL;
		if(token2 != NULL) free(token2);
	        argument_list = NULL;
		pipe_args = NULL;
		break;
	      }
	  }
       	} else { //begin input redirection
	  if(token[0] == '<'){
	    free(token);
	    token = get_next_token(tokenizer); 
	  char * token2 = get_next_token(tokenizer);
          if(!(is_valid_string(token))||(token[0] == '>')||(token[0]=='|')||(token[0] == '<')||(token[0] == '&')||(is_pipe)){ //invalid
	    fprintf(stderr,"Invalid input.\n");
            free_tokenizer(tokenizer);
	    tokenizer = NULL;
            if(token != NULL) free(token);
	    token = NULL;
            if(token2 != NULL) free(token2);
	    token2 = NULL;
            erase_list(argument_list);
	    argument_list = NULL;
	    erase_list(pipe_args);
	    pipe_args = NULL;
	    if(in_filename != NULL) free(in_filename);
	    in_filename = NULL;
	    if(out_filename != NULL) free(out_filename);
	    out_filename = NULL;
	    is_pipe = 0;
            break;
          } else {//token is the specified input file
	    if(is_valid_string(token2)){
	      if(token2[0] == '|'){
		in_filename = token;
		free(token2);
		is_pipe = 1;
		token = get_next_token(tokenizer);
		continue;
	      }
	      if(token2[0] == '<'){ //double redirection -- invalid
		fprintf(stderr,"Invalid redirection.\n");
		free_tokenizer(tokenizer);
		tokenizer = NULL;
		free(token);
		token = NULL;
		free(token2);
		erase_list(argument_list);
		argument_list = NULL;
		if(in_filename != NULL) free(in_filename);
		in_filename = NULL;
		if(out_filename != NULL) free(out_filename);
		out_filename = NULL;
		break;
	      } else if (string_compare(token2,"&")) { //should execute input redirection in background
		int status = execute_args(argument_list,pipe_args,token,NULL,1);
		if(status > 0){//was started in background and executed successfully
		  was_background = 1;
		  last_backgrounded = status;
		  erase_list(argument_list);
		  erase_list(pipe_args);
		  if(background == NULL){
		    background = map_init(last_backgrounded, buf);
		  } else {
		    push_map(last_backgrounded, buf, background);
		  }
		  printf("\nStopped: %s\n",buf);
		  argument_list = NULL;
		  pipe_args = NULL;
		  bg_is_running = 0;
		}
		free_tokenizer(tokenizer);
		tokenizer = NULL;
		free(token);
		token = NULL;
		free(token2);
		break;
	      } else { //token 2 after a < is something other than < or |
		if(token2[0] == '>'){ //valid combined redirection
		  char* token3 = get_next_token(tokenizer); //possible input
		  if(!is_valid_string(token3)||(is_valid_string(token3) &&(token3[0]=='&'))){
		    fprintf(stderr,"Invalid input redirection.\n");
		    free_tokenizer(tokenizer);
		    tokenizer = NULL;
		    free(token);
		    token = NULL;
		    free(token2);
		    if(token3 != NULL) free(token3);
		    erase_list(argument_list);
		    argument_list = NULL;
		    break;
		  } else { //token3 is the specified output file, we are redirecting both input and output
		    char* token4 = get_next_token(tokenizer);
		    int status;
		    if(is_valid_string(token4) && (string_compare(token4,"&"))){
		      status = execute_args(argument_list,pipe_args,token,token3,1);
		    } else {
		      status = execute_args(argument_list,pipe_args,token,token3,0);
		    }
		    if(status > 0){//sent SIGTSTP or was started in background
		      was_background = 1;
		      last_backgrounded = status;
		      erase_list(argument_list);
		      erase_list(pipe_args);
		      if(background == NULL){
			background = map_init(last_backgrounded, buf);
		      } else {
			push_map(last_backgrounded, buf, background);
		      }
		      printf("\nStopped: %s\n",buf);
		      argument_list = NULL;
		      pipe_args = NULL;
		      bg_is_running = 0;
		    } else {
		      erase_list(argument_list);
		      argument_list = NULL;
		      erase_list(pipe_args);
		      pipe_args = NULL;
		    }
		    free_tokenizer(tokenizer);
		    tokenizer = NULL;
		    free(token);
		    token = NULL;
		    free(token2);
		    free(token3);
		    break;
		  }
		} else { //output is something invalid
		  fprintf(stderr,"invalid input redirection.\n");
		  free_tokenizer(tokenizer);
		  tokenizer = NULL;
		  free(token);
		  token = NULL;
		  free(token2);
		  erase_list(argument_list);
		  argument_list = NULL;
		  break;
		}
	      }
	    } else { //token2 is invalid or null so we are just redirecting input from token, not output
		int status = execute_args(argument_list,pipe_args,token,NULL,0);//execution failed
		if(status > 0){//sent SIGTSTP
		  was_background = 1;
		  last_backgrounded = status;
		  erase_list(argument_list);
		  erase_list(pipe_args);
		  if(background == NULL){
		    background = map_init(last_backgrounded, buf);
		  } else {
		    push_map(last_backgrounded, buf, background);
		  }
		  printf("\nStopped: %s\n",buf);
		  argument_list = NULL;
		  pipe_args = NULL;
		  bg_is_running = 0;
		} else {
		  erase_list(argument_list);
		  argument_list = NULL;
		  erase_list(pipe_args);
		  pipe_args = NULL;
		}
		free_tokenizer(tokenizer);
		tokenizer = NULL;
	        if(token != NULL) free(token);
		if(token2 != NULL) free(token2);
		break;
	      }
	    }
	  } else { //some argument or program
	    if(!is_pipe){
	      push(token,argument_list);
	    } else {
	      if(pipe_args == NULL){
		pipe_args = list_init(token);
	      } else {
		push(token,pipe_args);
	      }
	    }
	  }
	  token = get_next_token(tokenizer); //get all tokens from current input
	}
      }
      if(is_pipe && (pipe_args == NULL)&&(!is_valid_string(token) && (tokenizer != NULL))){ //nothing after the pipe symbol
	fprintf(stderr,"Invalid use of pipe operator.\n");
	erase_list(argument_list);
	argument_list = NULL;
	if(in_filename != NULL) free(in_filename);
	in_filename = NULL;
	if(out_filename != NULL) free(out_filename);
	out_filename = NULL;
	free_tokenizer(tokenizer);
	continue;
      }
      if((!was_background)&&((argument_list!=NULL)&&(!is_pipe||(pipe_args!=NULL)))){
	if(token != NULL) free(token);
	int status = execute_args(argument_list,pipe_args,in_filename,out_filename,0);
	if(status > 0){//sent SIGTSTP
	  last_backgrounded = status;
	  erase_list(argument_list);
	  erase_list(pipe_args);
	  if(background == NULL){
	    background = map_init(last_backgrounded, buf);
	  } else {
	    push_map(last_backgrounded, buf, background);
	  }
	  printf("\nStopped: %s\n",buf);
	  argument_list = NULL;
	  pipe_args = NULL;
	  bg_is_running = 0;
	  free_tokenizer(tokenizer);
	  continue;
	} else {
	  if(in_filename != NULL) free(in_filename);
	  in_filename = NULL;
	  if(out_filename != NULL) free (out_filename);
	  out_filename = NULL;
	  erase_list(argument_list);
	  argument_list = NULL;
	  erase_list(pipe_args);
	  pipe_args = NULL;
	  free_tokenizer(tokenizer);
	}
      }
    } else { //null or empty token
      if(token != NULL) free(token);
      free_tokenizer(tokenizer);
    }
    if(last_backgrounded||((background != NULL) && (background->size > 0))){//signal handling
      int status=0;
      pid_t status_pid;
      while((status_pid = waitpid(-1,&status,WNOHANG|WUNTRACED)) > 0){//poll for bg process state change
	if(WIFEXITED(status)){
	  printf("\nFinished: %s\n",get_name(status_pid,background));
	} else if(WIFSTOPPED(status)){
	  printf("\nStopped: %s\n",get_name(status_pid,background));
	} else if(WIFCONTINUED(status)){
	  printf("Continued: %s\n",get_name(status_pid,background));
	} else {
	  continue;
	}
	if(WIFEXITED(status)){
	  last_backgrounded = 0;
	  bg_is_running = 0;
	  map_remove(status_pid,background);
	}
	if(WIFSTOPPED(status)){ bg_is_running = 0;
	  if(WIFCONTINUED(status)) bg_is_running = 1;
	}
	status_pid = waitpid(-1,&status,WNOHANG|WUNTRACED);
      }
    }
  }
  if(close(controlling_terminal) == -1){
    perror("closing terminal file");
    free(blocked_sigs);
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
}
