#include <stdio.h>
#include <stdlib.h>

struct node{
  char *string;
  struct node *next;
  struct node *prev;
};

struct linked_list{
  struct node *head;
  int size;
};

struct linked_list* list_init(char* s)
{
    struct node *head_node;
    struct linked_list *list;

    if((head_node = (struct node *) malloc (sizeof(struct node))) == NULL){
      fprintf(stderr,"node malloc failed in list_init.");
      return NULL;
    }
    head_node -> next = NULL;
    head_node -> prev = NULL;
    head_node -> string = s;
     if((list = (struct linked_list *) malloc (sizeof(struct linked_list))) == NULL){
      fprintf(stderr,"list malloc failed in list_init.");
      return NULL;
    }
     list -> head = head_node; 
     int list_size = 1;
     list -> size = list_size;
    return list;
}

char* peek(struct linked_list* list){
  struct node* currNode = list->head;
  if(currNode != NULL){
    while(currNode -> next != NULL){
      currNode = currNode -> next;
    }
    return currNode -> string;
  }
  return 0;
}
   
struct linked_list* push(char* s,struct linked_list* list){
  struct node* prev_node = NULL; //init null in case of singleton list
  struct node* curr_node = list->head;

  if(curr_node != NULL){
    while(curr_node->next != NULL){
      prev_node = curr_node;
      curr_node = curr_node -> next;
    }
  } else { //case of empty list
     if((curr_node = malloc(sizeof(struct node))) == NULL){
       fprintf(stderr,"malloc failed in push().");
       return NULL;
     }
     curr_node -> string = s;
     curr_node -> next = NULL;
     curr_node -> prev = NULL;
     list -> size = 1;
     list -> head = curr_node;
     return list;
  }

  if((curr_node -> next = malloc(sizeof(struct node))) == NULL){
    fprintf(stderr,"nonempty list malloc failed in push().");
    return NULL;
  }
  prev_node = curr_node;
  curr_node = curr_node->next;
  if(curr_node == NULL){
    fprintf(stderr,"Error in linkedlist: could not push new node.");
    return 0;
  }
  curr_node -> prev = prev_node;
  curr_node -> next = NULL;
  curr_node -> string = s;
  list -> size = (list->size)+1;
  return list;
}

char* pop_head(struct linked_list* list){
  if((list == NULL) || (list->head == NULL)) return NULL;
  char* to_return = list->head->string;
  free(list->head);
  list->head = list->head->next;
  if(list->head != NULL){//case of singleton list
    list->head->prev = NULL;
  }
  list->size = list->size-1;
  return to_return;
}

void erase_list(struct linked_list* list)
{
  if(list == NULL) return;
  struct node *prev_node = list -> head;
  struct node *curr_node = list -> head;
  while(curr_node != NULL){ //free all elements
    if(curr_node->string != NULL) free(curr_node->string);
    prev_node = curr_node;
    curr_node = curr_node -> next;
    free(prev_node);
  }
  free(list);
}

char* pop(struct linked_list* list){
  if((list == NULL)||(list->head==NULL)){
    return 0;
  }

  struct node* curr_node = list->head->next;
  struct node* prev_node = list->head;
  
  char* to_return=list->head->string; //case of singleton list
  if(curr_node != NULL){
    while(curr_node->next != NULL){
      prev_node = curr_node;
      curr_node = curr_node -> next;
    }
    to_return = curr_node->string;
    free(curr_node);
  }
  prev_node->next = NULL;
  list->size = (list->size) - 1;
  return to_return;
}

void print_list(struct linked_list* list){
  if(list == NULL){
    fprintf(stderr,"Null list input in print_list().");
    return;
  }
  if(list->size == 0){
    return;
  }
  struct node* currNode = list->head;
  int count = 0;
  while(currNode->next != NULL){
    fprintf(stdout,"%s ",currNode->string);
    count++;
    currNode = currNode -> next;
  }
  fprintf(stdout,"%s",currNode->string);
  return;
}

int list_size(struct linked_list* list){
  if(list==NULL){
    return 0;
  }
  return list->size;
}
