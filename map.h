struct map_node{
  pid_t *pid;
  char* name;
  struct map_node *next;
};

struct map{
  struct map_node *head;
  int size;
};

struct map* map_init(pid_t new_pid, char* input);

void push_map(pid_t new_pid, char* input, struct map* target_map);

void map_remove(pid_t target_pid, struct map* target_map);

char* get_name(pid_t target_pid, struct map* target_map);

void erase_map(struct map* map);

int map_size(struct map* map);
