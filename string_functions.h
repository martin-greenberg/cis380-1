int string_length(char *s);

char* string_duplicate(char* s);

int is_valid_string(char* s);

int string_compare(char *s1, char *s2);
