CFLAGS=-g -Wall
CC=gcc
SRCS=tokenizer.c token-shell.c
OBJS=tokenizer.o token-shell.o
LDFLAGS=
LIBS=

all:    shell

shell: tokenizer.o linkedlist.o string_functions.o map.o shell.o
	$(CC) tokenizer.o shell.o linkedlist.o string_functions.o map.o -o shell

linkedlist.o : linkedlist.c
	$(CC) -c linkedlist.c

$(SRCS):
	$(CC) $(CFLAGS) -c $*.c

tokenizer.o:
	$(CC) -c tokenizer.c

string_functions.o:
	$(CC) -c string_functions.c

map.o: linkedlist.o
	$(CC) -c map.c

clean:
	rm -f *.o shell
