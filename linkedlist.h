struct node{
    char *string;
    struct node *next;
  struct node *prev;
};

struct linked_list{
  struct node *head;
  int size;
};

struct linked_list* list_init(char* s);

char* peek(struct linked_list* list);
   
struct linked_list* push(char* s,struct linked_list* list);

char* pop_head(struct linked_list* list);

void erase_list(struct linked_list* list);

char* pop(struct linked_list* list);

void print_list(struct linked_list* list);

int list_size(struct linked_list* list);
