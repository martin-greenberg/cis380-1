#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "string_functions.h"

struct map_node{
  pid_t pid;
  char *name;
  struct map_node *next;
};

struct map{
  struct map_node *head;
  int size;
};

struct map* map_init(pid_t new_pid, char* input)
{
    struct map_node *head_node;
    struct map *new_map;

    if((head_node = (struct map_node *) malloc (sizeof(struct map_node))) == NULL){
      fprintf(stderr,"map node malloc failed in list_init.");
      return NULL;
    }
    head_node -> next = NULL;
    head_node -> pid = new_pid;
    char* node_name;
    if((node_name = string_duplicate(input)) == NULL){//string malloc failed
      fprintf(stderr,"malloc failed in map_init");
      return NULL;
    }
    head_node -> name = node_name;
    if((new_map = (struct map *) malloc (sizeof(struct map))) == NULL){
      fprintf(stderr,"list malloc failed in map_init.");
      return NULL;
    }
    new_map -> head = head_node; 
    new_map -> size = 1;
    return new_map;
}
   
int push_map(pid_t new_pid, char* input, struct map* target_map){
  struct map_node* curr_node = target_map -> head;
  char* node_name;
  if((node_name = string_duplicate(input)) == NULL){
    fprintf(stderr,"malloc failed in push_map()");
    return;
  }

  if(curr_node != NULL){
    while(curr_node->next != NULL){
      curr_node = curr_node -> next;
    }
  } else { //case of empty list
     if((curr_node = malloc(sizeof(struct map_node))) == NULL){
       fprintf(stderr,"malloc failed in push().");
       free(node_name);
       return;
     }
     curr_node -> name = node_name;
     curr_node -> pid = new_pid;
     curr_node -> next = NULL;
     target_map -> size = 1;
     target_map -> head = curr_node;
     return;
  }

  if((curr_node -> next = malloc(sizeof(struct map_node))) == NULL){
    fprintf(stderr,"nonempty map: malloc failed in push().");
    free(node_name);
    return;
  }
  curr_node = curr_node->next;
  curr_node -> next = NULL;
  curr_node -> pid = new_pid;
  curr_node -> name = node_name;
  target_map -> size = (target_map->size)+1;
  return;
}

void map_remove(pid_t target_pid, struct map* target_map){
  if((target_map == NULL) || (target_map->head == NULL)) return;
  struct map_node *prev_node = target_map->head;
  struct map_node *curr_node = target_map->head->next;
  if(prev_node->pid == target_pid){//if target node is head
    target_map->head = curr_node;
    target_map->size = target_map->size-1;
    free(prev_node->name);
    free(prev_node);
    return;
  }
  while(curr_node->pid != target_pid){
    if(curr_node->next == NULL){//not found
      return;
    }
    prev_node = curr_node;
    curr_node = curr_node->next;
  }
  prev_node->next = curr_node->next;
  free(curr_node->name);
  free(curr_node);
  target_map->size = target_map->size-1;
  return;
}

char* get_name(pid_t target_pid, struct map* target_map){
  if((target_map == NULL) || (target_map->head == NULL)) return;
  struct map_node *curr_node = target_map->head;
  if(curr_node->pid == target_pid){
    return curr_node->name;
  }
  while(curr_node->pid != target_pid){
    if(curr_node->next == NULL){//not found
      return NULL;
    }
    curr_node = curr_node->next;
  }
  return curr_node->name;
}

void erase_map(struct map* map)
{
  if(map == NULL) return;
  while(map->head != NULL){
    map_remove(map->head->pid,map);
  }
  free(map);
}

int map_size(struct map* map){
  if(map==NULL){
    return 0;
  }
  return map->size;
}
