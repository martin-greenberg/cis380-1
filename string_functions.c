#include <stdio.h>
#include <stdlib.h>

int string_length(char *s)
{
  if(s == NULL) {
    return 0;
  }
  int x = 0;
  while(s[x] != '\0'){
    x++;
  }
  return x;
}

char* string_duplicate(char* s)
{
  char* to_return;
  int len = string_length(s);
  if((to_return = malloc(len+1)) == NULL){
    fprintf(stderr, "Could not malloc in string_duplicate().");
    return NULL;
  }
  int i;
  for(i=0;i<=len;i++){
    to_return[i]=*s;
    s++;
  }
  return to_return;
}

int is_valid_string(char* s)
{
  if(s == NULL) return 0;
  if((s[0]==0)||(s[0] < 32)||(s[0]>126)) return 0;
  int i;
  for(i = 1; *(&s+i) != NULL; i++){
    if(s[i] == 0) return 1; //end of input
    if((s[i] < 32)||(s[i]>126)) return 0; //invalid character
  }
  return 1;
}

int string_compare(char *s1, char *s2){
  int s1_len = string_length(s1);
    int s2_len = string_length(s2);
    if(s1_len != s2_len) return 0;
    int i;
    for(i = 0; i < string_length(s1);i++){
      if(s1[i]!=s2[i]){
	return 0;
      }
    }
    return 1;
}
